"use strict";


module.exports = class blackbox {

    constructor(){

        this.apiv1 = 'https://blackbox.ipinfo.app/lookup/';
        this.apiv2 = 'https://blackbox.ipinfo.app/api/v2/';
        this.r = require('axios');

    }

    /**
     * Uses the v1 api to determine if the ip address is a likely vpn.
     * @param {string} ip address you wish to determine if is likely a vpn.
     */
    isVPN(ip){
        return new Promise((resolve, reject) => {
            this.r.get(this.apiv1 + ip)
            .then((response) => {
                if(response.data == "Y"){
                    resolve(true);
                }
                else if(response.data == "N"){
                    resolve(false);
                }
                else{
                    reject("Error: Invalid Reponse");
                }
            })
            .catch((error) => {
                reject(error);
            })
        });
    }

    /**
     * Get version 2 api.
     * @param {string} ip address you wish to lookup.
     */
    get(ip){
        return new Promise((resolve, reject) => {

            this.r.get(this.apiv2 + ip)
            .then(response => {
                resolve(response.data);
            })
            .catch(error => {
                reject(error);
            })
        })
    }

    isTor(ip){
        return new Promise((resolve, reject) => {
            this.get(ip)
            .then(response => {
                if(response.detection.tor){
                    resolve(true);
                }
                else if(!response.detection.tor){
                    resolve(false);
                }
                else{
                    reject("Error: Invalid Response");
                }
            })
            .catch(error => {
                reject(error);
            })
        })
    }

    isHosting(ip){
        return new Promise((resolve, reject) => {
            this.get(ip)
            .then(response => {
                if(response.detection.hosting){
                    resolve(true);
                }
                else if(!response.detection.hosting){
                    resolve(false);
                }
                else{
                    reject("Error: Invalid Response");
                }
            })
            .catch(error => {
                reject(error);
            })
        })
    }

    isProxy(ip){
        return new Promise((resolve, reject) => {
            this.get(ip)
            .then(response => {
                if(response.detection.proxy){
                    resolve(true);
                }
                else if(!response.detection.proxy){
                    resolve(false);
                }
                else{
                    reject("Error: Invalid Response");
                }
            })
            .catch(error => {
                reject(error);
            })
        })
    }

    isBogon(ip){
        return new Promise((resolve, reject) => {
            this.get(ip)
            .then(response => {
                if(response.detection.bogon){
                    resolve(true);
                }
                else if(!response.detection.bogon){
                    resolve(false);
                }
                else{
                    reject("Error: Invalid Response");
                }
            })
            .catch(error => {
                reject(error);
            })
        })
    }

    asn(ip){
        return new Promise((resolve, reject) => {
            this.get(ip)
            .then(response => {
                if(response.asn){
                    resolve(response.asn);
                }
                else{
                    reject("Error: Invalid Response");
                }
            })
            .catch(error => {
                reject(error);
            })
        })
    }

    score(ip){
        return new Promise((resolve, reject) => {
            this.get(ip)
            .then(response => {
                if(response.score){
                    resolve(response.score);
                }
                else{
                    reject("Error: Invalid Response");
                }
            })
            .catch(error => {
                reject(error);
            })
        })
    }

}